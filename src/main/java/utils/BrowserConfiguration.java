package utils;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import base.TestBase;

public class BrowserConfiguration extends TestBase{
	private static final String PARAM_PROPERTY_FILE_DIR="./resources//parameters/";
	private static final String PARAM_PROPERTY_FILE_EXTENSION=".params.properties";
	private static final String TARGET_URL="target_url";
	private static final String OS_NAME="os_name";
	private static final String BROWSER_NAME="browser_name";
	public static WebDriver driver;
	public static String osName="";
	public static String targetUrl="";
	public static String browserName="";

	@BeforeSuite(alwaysRun=true)
	public void loadParameters() {
		loadClassPathProperties(PARAM_PROPERTY_FILE_DIR,PARAM_PROPERTY_FILE_EXTENSION);
	}

	@BeforeSuite(alwaysRun=true)
	public void loadConfigProperties() {
		loadClassPathProperties(PARAM_PROPERTY_FILE_DIR,PARAM_PROPERTY_FILE_EXTENSION);

		if(System.getProperty(BROWSER_NAME)!=null)
			browserName=System.getProperty(BROWSER_NAME);

		if(System.getProperty(OS_NAME)!=null)
			osName=System.getProperty(OS_NAME);

		if(System.getProperty(TARGET_URL)!=null)
			targetUrl=System.getProperty(TARGET_URL);
	}

	@BeforeSuite(alwaysRun=true, timeOut = 25000, dependsOnMethods = {"loadParameters","loadConfigProperties"})
	public void launchBrowser() {
		if(osName.contains("Windows")) {
			if(browserName.equalsIgnoreCase("Firefox")) {
				System.setProperty("webdriver.gecko.driver", "./drivers//geckodriver.exe");
				driver=new FirefoxDriver();
			}else if(browserName.equalsIgnoreCase("Chrome")) {
				System.setProperty("webdriver.chrome.driver", "./drivers//chromedriver.exe");
				driver=new ChromeDriver();
			}else if(browserName.equalsIgnoreCase("Edge")) {
				System.setProperty("webdriver.edge.driver", "./drivers//MicrosoftWebDriver.exe");
				driver=new EdgeDriver();
			}else if(browserName.equalsIgnoreCase("IE")) {
				System.setProperty("webdriver.ie.driver", "drivers//IEDriverServer.exe");
				driver=new InternetExplorerDriver();
			}
		}else if(osName.contains("Ubuntu")|| (osName.contains("Linux"))) {
			if(browserName.equalsIgnoreCase("Chrome")) {
				System.setProperty("webdriver.chrome.driver", "./drivers//chromedriver");
				driver=new ChromeDriver();
			}else if(browserName.equalsIgnoreCase("Firefox")) {
				System.setProperty("webdriver.gecko.driver", "./drivers//geckodriver-linux");
				driver=new FirefoxDriver();
			}

		}
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().deleteAllCookies();
		driver.manage().window().maximize();
		driver.navigate().to(targetUrl);
		driver.navigate().refresh();
	}
	
	@AfterSuite(alwaysRun=true)
	public void quitDriver() {
		if(driver!=null) {
			driver.quit();
		}
	}
}
