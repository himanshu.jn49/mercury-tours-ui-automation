package utils;

import java.io.File;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.io.FileHandler;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

public class ScreenshotUtils implements ITestListener {
	private static final String screenShotPath=System.getProperty("user.dir")+"//target//Test_ExecutionResult/";
	private static String testStatus="passed";

	@Override
	public void onTestStart(ITestResult result) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTestSuccess(ITestResult result) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTestFailure(ITestResult result) {
		try {
			testStatus="failed";
			File screenShot=((TakesScreenshot)BrowserConfiguration.driver).getScreenshotAs(OutputType.FILE);
			if(screenShot!=null) {
				FileHandler.copy(screenShot, new File(result.getName()+".png"));
				System.out.println("**** Placed screenshot at: file:"+result.getName()+".png ****");
			}
		}catch(Exception e) {
			testStatus="failed";
			System.out.println("#### Unable to take screenshot at: file:"+result.getName()+".png ####");
		}
	}

	@Override
	public void onTestSkipped(ITestResult result) {
		try {
			testStatus="failed";
			File screenShot=((TakesScreenshot)BrowserConfiguration.driver).getScreenshotAs(OutputType.FILE);
			if(screenShot!=null) {
				FileHandler.copy(screenShot, new File(result.getName()+".png"));
				System.out.println("**** Placed screenshot at: file:"+result.getName()+".png ****");
			}
		}catch(Exception e) {
			testStatus="failed";
			System.out.println("#### Unable to take screenshot at: file:"+result.getName()+".png ####");
		}
	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStart(ITestContext context) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onFinish(ITestContext context) {
		// TODO Auto-generated method stub
		
	}

}
