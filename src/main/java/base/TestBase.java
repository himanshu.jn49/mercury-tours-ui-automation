package base;

import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class TestBase {
	private static final Properties loadProperties(InputStream input) {
		if(input==null)
			throw new IllegalArgumentException("Must have a valid property file to load");
		Properties properties=new Properties();
		try {
			properties.load(input);
		}catch(IOException e) {
			throw new RuntimeException("Unable to load the requested property file", e);
		}finally {
			try {
				input.close();
			}catch(IOException e) {
				e.printStackTrace();
			}
		}
		return properties;
	}
	
	protected void loadClassPathProperties(final String resourceFileDir, final String resourceFileExtension) {
		String path=getClass().getClassLoader().getResource("parameters/").getPath();
		File[] propertiesFile=new File(path).listFiles(new FilenameFilter() {
			
			@Override
			public boolean accept(File dir, String name) {
				// TODO Auto-generated method stub
				return name.endsWith(resourceFileExtension);
			}
		});
		try {
			for(File file:propertiesFile) {
				Properties properties=loadProperties(new FileInputStream(file));
				for(String key:properties.stringPropertyNames()) {
					System.setProperty(key, properties.getProperty(key));
				}
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

}
